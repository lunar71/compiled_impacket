# most of the virtualenv code from
# https://stackoverflow.com/questions/57593111/how-to-call-pip-from-a-python-script-and-make-it-install-locally-to-that-script
import subprocess
import glob
import sys
import os
from base64 import b64encode


class VirtualEnvironment(object):
    def __init__(self, venv_dir):
        self.venv_dir = venv_dir
        if os.name == 'nt':
            self.venv_python = os.path.join(self.venv_dir, 'Scripts', 'python.exe')
        elif os.name == 'posix':
            self.venv_python = os.path.join(self.venv_dir, 'bin', 'python')

    def pip_install(self, package, extra_args=None):
        args = [sys.executable, '-m', 'pip', 'install', package]
        if extra_args:
            args.extend(extra_args)
        print(args)
        subprocess.call(args)

    def create_venv(self):
        self.pip_install('virtualenv')

        if not os.path.exists(self.venv_dir):
            subprocess.call([sys.executable, '-m', 'virtualenv', self.venv_dir])

    def is_venv(self):
        return sys.prefix == self.venv_dir

    def activate_venv(self):
        subprocess.call([self.venv_python, __file__] + sys.argv[1:])
        exit(0)

    def run(self):
        if not self.is_venv():
            self.create_venv()
            self.activate_venv()

def freeze():
    pwd = os.path.dirname(os.path.realpath(__file__))
    venv = VirtualEnvironment(os.path.join(pwd, 'venv'))

    venv.run()
    venv.pip_install('wheel')
    if os.name == 'nt':
        venv.pip_install('pyreadline')
        extra = ["--global-option=build_ext", r"--global-option=--library-dirs=C:\WdpPack\Lib;C:\Program Files (x86)\Windows Kits\10\Lib\10.0.19041.0\um\x64;C:\Program Files (x86)\Windows Kits\10\Lib\10.0.19041.0\ucrt\x64", r"--global-option=--include-dirs=C:\WdpPack\Include;C:\Program Files (x86)\Windows Kits\10\Include\10.0.19041.0\um;C:\Program Files (x86)\Windows Kits\10\Include\10.0.19041.0\shared;C:\Program Files (x86)\Windows Kits\10\Include\10.0.19041.0\ucrt;C:\Program Files (x86)\Windows Kits\10\bin\10.0.19041.0\x64"]
        venv.pip_install('pcapy', extra_args=extra)
    elif os.name == 'posix':
        venv.pip_install('pcapy')
    venv.pip_install('pyinstaller')
    venv.pip_install('impacket')

    try:
        import PyInstaller.__main__
    except Exception as e:
        print('Error:\n{}'.format(e))

    if os.name == 'nt':
        impacket_examples = os.path.join(pwd, 'venv', 'Scripts', '*.py')
    elif os.name == 'posix':
        impacket_examples = os.path.join(pwd, 'venv', 'bin', '*.py')

    for each in glob.glob(impacket_examples):
        if os.path.basename(each).startswith('activate_this'):
            continue
        if os.name == 'nt':
            outfile = os.path.basename(each)
        elif os.name == 'posix':
            outfile = '{}.bin'.format(os.path.basename(each))

        PyInstaller.__main__.run([
            '--onefile',
            '--clean',
            '--specpath', os.path.join(pwd, 'specs'),
            '--distpath', os.path.join(pwd, 'bin'),
            '--name', outfile,
            each
        ])

    #venv_path = os.path.join(os.path.dirname(__file__), 'venv')
    #impacket_examples = ['GetADUsers.py','GetNPUsers.py','GetUserSPNs.py','addcomputer.py','atexec.py','dcomexec.py','dpapi.py','esentutl.py','exchanger.py','findDelegation.py','getArch.py','getPac.py','getST.py',#'getTGT.py','goldenPac.py','karmaSMB.py','kintercept.py','lookupsid.py','mimikatz.py','mqtt_check.py','mssqlclient.py','mssqlinstance.py','netview.py','nmapAnswerMachine.py','ntfs-read.py','ntlmrelayx.py','ping.py',#'ping6.py','psexec.py','raiseChild.py','rdp_check.py','reg.py','registry-read.py','rpcdump.py','rpcmap.py','sambaPipe.py','samrdump.py','secretsdump.py','services.py','smbclient.py','smbexec.py','smbpasswd.py',#'smbrelayx.py','smbserver.py','sniff.py','sniffer.py','split.py','ticketConverter.py','ticketer.py','wmiexec.py','wmipersist.py','wmiquery.py']
    #
    #for each in impacket_examples:
    #    if os.name == 'nt':
    #        impacket_example = os.path.join(venv_path, 'Scripts', each)
    #    elif os.name == 'posix':
    #        impacket_example = os.path.join(venv_path, 'bin', each)
    #
    #    PyInstaller.__main__.run([
    #        '--onefile',
    #        '--clean',
    #        '--specpath', os.path.join(pwd, 'specs'),
    #        '--distpath', os.path.join(pwd, 'bin'),
    #        impacket_example
    #    ])

def create_stagers():
    windows_bin = glob.glob(os.path.join(os.getcwd(), 'bin', '*.exe'))
    for each in windows_bin:
        with open(each, 'rb') as infile:
            b64enc = b64encode(infile.read())

            with open('{}_b64.ps1'.format(each), 'w') as outfile:
                outfile.write('$x = "{}"\n\n'.format(b64enc.decode('UTF-8')))
                outfile.write('[IO.File]::WriteAllBytes(".\{}", [Convert]::FromBase64String($x))'.format(os.path.basename(each)))

    linux_bin = glob.glob(os.path.join(os.getcwd(), 'bin', '*.bin'))
    for each in linux_bin:
        with open(each, 'rb') as infile:
            b64enc = b64encode(infile.read())

            with open('{}_b64.sh'.format(each), 'w') as outfile:
                outfile.write('X="{}"\n\n'.format(b64enc.decode('UTF-8')))
                outfile.write('base64 -d <<< "$X" > ./{}'.format(os.path.basename(each)))

if __name__ == '__main__':
    freeze()
    create_stagers()
